from django.db import models

# Create your models here.
class Qbank(models.Model):
	Qtext = models.TextField(null=False)
	Marks = models.IntegerField(null=False)
	Difficulty = models.CharField(max_length=10)