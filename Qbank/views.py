import json
from django.shortcuts import render
from models import Qbank

# Create your views here.
def index(request):
	try:
		#Queries for Section-A
		A_easy = Qbank.objects.filter(Difficulty='Easy', Marks=1).order_by('?')[:4]
		A_medium = Qbank.objects.filter(Difficulty='Medium', Marks=1).order_by('?')[:2]
		A_hard = Qbank.objects.filter(Difficulty='Hard', Marks=1).order_by('?')[:4]

		#Queries for Section-B
		B_easy = Qbank.objects.filter(Difficulty='Easy', Marks=5).order_by('?')[:2]
		B_medium = Qbank.objects.filter(Difficulty='Medium', Marks=5).order_by('?')[:2]

		#Queries for Section-C
		C_medium = Qbank.objects.filter(Difficulty='Medium', Marks=10).order_by('?')[:3]
		C_hard = Qbank.objects.filter(Difficulty='Hard', Marks=10).order_by('?')[:1]

		context_data = {'A_easy': A_easy, 'A_medium': A_medium, 'A_hard': A_hard, 'B_easy': B_easy, 'B_medium': B_medium, 'C_medium': C_medium, 'C_hard': C_hard}
		return render(request, 'Qbank/questions.html', context_data)
	except Exception as e:
		print('Error: %s' % e)
	